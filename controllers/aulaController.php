<?php 

require_once 'models/aula.php';
require_once 'models/alumno.php';
require_once 'models/relacion.php';
require_once 'models/posicion.php';

class aulaController{

	public function index(){
		$aula=new aula();
		$aulas=$aula->aulas();
		require_once 'views/aula/aulas.php';
	}
	

	public function ver(){
		$aula=new aula();
		$aula->setId_aula($_GET['id_aula']);
		$clase=$aula->unAula();
		$alumno=new alumno();
		$alumno->setId_aula($_GET['id_aula']);
		$alumnos=$alumno->alumnosClase();
		require_once 'views/aula/aula.php';
	}

	public function ajax(){
		$posicion=new posicion();
		$posicion->setId_alumno($_POST['id']);
		$posicion->setCoordx($_POST['x']);
		$posicion->setCoordy($_POST['y']);
		$posicion->setGuardado("sin");
		$posicion->mover();
	}

	public function aulas(){
		$aula=new aula();
		$aulas=$aula->aulas();
		require_once 'views/aula/aulas.php';
	}

	function crear(){
			 require_once 'views/aula/crear.php';
	}

	public function save(){
		if (isset($_POST)) {
			
			$codigo = isset($_POST['codigo']) ? $_POST['codigo'] : false;
			$mesas = isset($_POST['mesas']) ? $_POST['mesas'] : false;
			


			if($codigo && $mesas){
				$aula= new Aula();
				$aula->setCodigo($codigo);
				$aula->setMesas($mesas);
				
				$save= $aula->save();
			
				if ($save) {
					$_SESSION['register']= "complete" ;
				}else{
					$_SESSION['register']= "failed";
				}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
		
		header("Location:".base_url.'aula/aulas');
	}

	public function borrar(){
			if (isset($_GET['id_aula'])) {
				$id_aula=$_GET['id_aula'];
				$aula= new Aula();
				$aula->setId_aula($id_aula);
				$delete=$aula->borrarAula();
				if ($delete) {
					$_SESSION['delete']='complete';
				}else{
					$_SESSION['delete']='failed';
				}

			}else{
				$_SESSION['delete']='failed';
			}


		header("Location:".base_url."aula/aulas");
	}




}

?>