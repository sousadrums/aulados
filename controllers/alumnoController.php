<?php 

require_once 'models/alumno.php';
require_once 'models/relacion.php';

class alumnoController{
	

	public function index(){
		if (isset($_SESSION['id'])) {
			unset($_SESSION['id']);
		}
		$alumno=new alumno();
		$alumnos=$alumno->alumnosClases();
		
		require_once 'views/alumno/alumnos.php';
	}

	public function relaciones(){
		$id=$_GET['id_alumno'];
		if (!isset($_SESSION['alumno'])) {
			$_SESSION['id']=$id;
		}
		$relacion=new relacion();
		$relaciones=$relacion->relaciones();
		require_once 'views/relaciones/ver.php';
	}

	public function save(){
		if (isset($_POST)) {
			
			$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
			$apellidos = isset($_POST['apellidos']) ? $_POST['apellidos'] : false;
			//$foto = isset($_POST['foto']) ? $_POST['foto'] : false;


			if($nombre && $apellidos){
				$alumno= new Alumno();
				$alumno->setNombre($nombre);
				$alumno->setApellidos($apellidos);
				
				//guardar imagen
				$file=$_FILES['foto'];
				$filename=$file['name'];
				$mimetype=$file['type'];

			

				if ($mimetype=="image/jpeg" || $mimetype=="image/jpg" || $mimetype== "image/jpg"|| $mimetype== "image/png" || $mimetype== "image/gif") {
					if (!is_dir('uploads/images')) {
						mkdir('uploads/images', 0777, true);
					}
					move_uploaded_file($file['tmp_name'], 'uploads/images/'.$filename);
					$alumno->setFoto($filename);
			}

			$save= $alumno->save();
			
			if ($save) {
				$_SESSION['register']= "complete" ;
			}else{
				$_SESSION['register']= "failed";
			}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
		
		header("Location:".base_url.'alumno/index');
	}

	function crear(){
			 require_once 'views/alumno/crear.php';
	}

	function borrar(){
		if (isset($_GET['id_alumno'])) {
			$id_alumno=$_GET['id_alumno'];
			$alumno= new alumno();
			$alumno->setId_alumno($id_alumno);
			$delete=$alumno->borrarAlumno();
			if ($delete) {
				$_SESSION['delete']='complete';
			}else{
				$_SESSION['delete']='failed';
			}

		}else{
			$_SESSION['delete']='failed';
		}


		header("Location:".base_url."alumno/index");
	}


	function asignar(){
			if (isset($_GET['id_alumno'])) {
				$id_alumno=$_GET['id_alumno'];
				if (!isset($_SESSION['alumno'])) {
					$_SESSION['id_alumno']=$id_alumno;
				}
				$alumno= new alumno();
				$alumno->setId_alumno($id_alumno);
				$aulas=$alumno->aulas();
				require_once 'views/alumno/asignar.php';
			}
		
		
	}


	function escogerAula(){
		if (isset($_SESSION['id_alumno']) && isset($_POST['id_aula'])) {
			$id_alumno=$_SESSION['id_alumno'];
			$id_aula=$_POST['id_aula'];
			$alumno= new alumno();
			$alumno->setId_alumno($id_alumno);
			$alumno->setId_aula($id_aula);
			$alumno->asignarClase();
			unset($_SESSION['id_alumno']);	
		}
		header("Location:".base_url."alumno/index");
	}

	function relacion(){
		$relacion= new relacion();
		
	}

	function popOver(){
		require_once 'views/alumno/pop.php';		
	}

}

?>