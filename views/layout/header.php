
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no"/>
	<title>MI AULA</title>
	<link rel="icon" type="image/x-icon" href="<?=base_url?>assets/img/auladoslogo.jpg" />
	<link rel="stylesheet" type="text/css" href="<?=base_url?>assets/css/styles.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?=base_url?>assets/js/jquery-ui-1.12.1/jquery-ui.min.css">
	<link rel="stylesheet" href="<?=base_url?>assets/js/jquery-ui-1.12.1/jquery-ui.structure.min.css">
	<link rel="stylesheet" href="<?=base_url?>assets/js/jquery-ui-1.12.1/jquery-ui.theme.min.css">
	<script type="text/javascript" src="<?=base_url?>assets/js/jquery-ui-1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?=base_url?>assets/js/interface.js"></script>
</head>
<body>
	<div class="container">
		<!--Cabecera-->
		<nav class="navbar  navbar-expand-sm bg-dark navbar-dark">
			<a class="navbar-brand" href="#"><img class="logo" src="<?=base_url?>assets/img/auladoslogo.jpg" alt=""></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
   				<span class="navbar-toggler-icon"></span>
  			</button>
  			<div class="collapse navbar-collapse" id="collapsibleNavbar">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="<?=base_url?>aula/aulas">aulas</a></li>
					<li class="nav-item"><a class="nav-link" href="<?=base_url?>alumno/index">alumnos</a></li>
				</ul>
			</div>
		</nav>
	<br>
	<!--Cuerpo-->
	<div class="container">
	