<form action="<?=base_url?>alumno/escogerAula" method="POST" class="form-group">
	<select name="id_aula" class="form-control-sm">
		<?php while ($aula=$aulas->fetch_object()) : ?>
			<option value="<?=$aula->id_aula?>"><?=$aula->codigo?>
			</option>
		<?php endwhile; ?>
	</select>
	<button type="submit" class="btn btn-primary btn-sm">asignar</button>
</form>