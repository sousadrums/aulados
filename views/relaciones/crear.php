<?php $alumnor=$alumnox->fetch_object() ?>
<h3>Relaciones de <?=$alumnor->nombre?> <?=$alumnor->apellidos?></h3>
<form action="<?=base_url?>relacion/asignacion" method="POST" class="form-group">
	<select name="id_alumno" class="form-control-sm">
		<?php while ($alumno=$alumnos->fetch_object()) : ?>
			<option value="<?=$alumno->id_alumno?>"><?=$alumno->nombre?> <?=$alumno->apellidos?>
			</option>
		<?php endwhile; ?>
	</select>
	<select name="relacion" class="form-control-sm" >
		<option value="1">buena</option>
		<option value="2">mala</option>
	</select>
	<button type="submit" class="btn btn-primary btn-sm">asignar</button>
</form>