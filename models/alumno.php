<?php


class alumno{
	private $id_alumno;
	private $nombre;
	private $apellidos;
	private $foto;
	private $id_aula;



	private $db;

	public function __construct() {
		$this->db = Database::connect();
		}

	function getId_alumno(){
		return $this->id_alumno;
	}

	function getNombre(){
		return $this->nombre;
	}

	function getApellidos(){
		return $this->apellidos;
	}
	function getFoto(){
		return $this->foto;
	}
	function getId_aula(){
		return $this->id_aula;
	}



	function setId_alumno($id_alumno){
		$this->id_alumno=$id_alumno;
	}
	function setNombre($nombre){
		$this->nombre=$nombre;
	}
	function setApellidos($apellidos){
		$this->apellidos=$apellidos;
	}
	function setFoto($foto){
		$this->foto=$foto;
	}
	function setId_aula($id_aula){
		$this->id_aula=$id_aula;
	}

	public function save(){
		$sql = "INSERT INTO alumnos VALUES (NULL, '{$this->getNombre()}', '{$this->getApellidos()}', '{$this->getFoto()}',NULL)";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	public function relaciones(){
		$sql="SELECT relaciones.id_alumno AS identificador, relaciones.id_alumnodos, alumnos.nombre AS nombreAmigo, alumnos.apellidos AS apellidoAmigo, relaciones.relacion AS tipo FROM relaciones JOIN alumnos ON relaciones.id_alumnodos=alumnos.id_alumno ";
		$alumnos= $this->db->query($sql);
		
		return $alumnos;
	}

	public function alumnos(){
		$sql="SELECT * FROM alumnos";
		$alumnos= $this->db->query($sql);
		return $alumnos;
	}

	public function unAlumno(){
		$sql="SELECT * FROM alumnos WHERE id_alumno={$this->id_alumno}";
		$alumno= $this->db->query($sql);
		return $alumno;
	}

	public function alumnosClases(){
		$sql="SELECT * ,(SELECT GROUP_CONCAT(A2.nombre,' ', A2.apellidos SEPARATOR '<br>') FROM relaciones B INNER JOIN alumnos A1 ON A1.id_alumno=B.id_alumno INNER JOIN alumnos A2 ON B.id_alumnodos=A2.id_alumno WHERE A1.id_alumno=alumnos.id_alumno AND B.relacion=1) AS amigos, (SELECT GROUP_CONCAT(A2.nombre,' ', A2.apellidos SEPARATOR '<br>') FROM relaciones B INNER JOIN alumnos A1 ON A1.id_alumno=B.id_alumno INNER JOIN alumnos A2 ON B.id_alumnodos=A2.id_alumno WHERE A1.id_alumno=alumnos.id_alumno AND B.relacion=2) AS enemigos FROM alumnos LEFT JOIN aulas ON aulas.id_aula=alumnos.id_aula ORDER BY id_alumno";
		$alumnos= $this->db->query($sql);
		return $alumnos;
	}

	public function alumnosSinClase(){
		$sql="SELECT * FROM alumnos WHERE id_aula IS NULL";
		$alumnos= $this->db->query($sql);
		return $alumnos;
	}

	public function borrarAlumno(){
		$sql="DELETE FROM alumnos WHERE id_alumno={$this->id_alumno}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}

	public function alumnosClase(){
		$sql="SELECT * ,(SELECT GROUP_CONCAT(A2.nombre,' ', A2.apellidos SEPARATOR '<br>') FROM relaciones B INNER JOIN alumnos A1 ON A1.id_alumno=B.id_alumno INNER JOIN alumnos A2 ON B.id_alumnodos=A2.id_alumno WHERE A1.id_alumno=alumnos.id_alumno AND B.relacion=1) AS amigos, (SELECT GROUP_CONCAT(A2.nombre,' ', A2.apellidos SEPARATOR '<br>') FROM relaciones B INNER JOIN alumnos A1 ON A1.id_alumno=B.id_alumno INNER JOIN alumnos A2 ON B.id_alumnodos=A2.id_alumno WHERE A1.id_alumno=alumnos.id_alumno AND B.relacion=2) AS enemigos, (SELECT p.coordx FROM posiciones p INNER JOIN alumnos al ON al.id_alumno=p.id_alumno WHERE p.id_alumno=alumnos.id_alumno) AS coordx, (SELECT p.coordy FROM posiciones p INNER JOIN alumnos al ON al.id_alumno=p.id_alumno WHERE p.id_alumno=alumnos.id_alumno) AS coordy FROM alumnos INNER JOIN aulas ON aulas.id_aula=alumnos.id_aula WHERE alumnos.id_aula='{$this->getId_aula()}'";
		$alumnos= $this->db->query($sql);

		return $alumnos;
	}

	function asignarClase(){
		$sql="UPDATE alumnos SET id_aula={$this->getId_aula()} WHERE id_alumno='{$this->getId_alumno()}'";
		$save= $this->db->query($sql);
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	function aulas(){
		$sql="SELECT * FROM aulas";
		$aulas= $this->db->query($sql);
		return $aulas;
	}
	function aula(){
		$sql="SELECT * FROM aulas INNER JOIN alumnos ON aulas.id_aula=alumnos.id_aula ";
		$aula= $this->db->query($sql);
		return $aula;
	}
}

?>