<?php


class relacion{
	private $id_relacion;
	private $id_alumno;
	private $id_alumnodos;
	private $relacion;



	private $db;

	public function __construct() {
		$this->db = Database::connect();
		}

	function getId_relacion(){
		return $this->id_relacion;
	}

	function getId_alumno(){
		return $this->id_alumno;
	}

	function getId_alumnodos(){
		return $this->id_alumnodos;
	}
	
	function getRelacion(){
		return $this->relacion;
	}




	function setId_relacion($id_relacion){
		$this->id_relacion=$id_relacion;
	}

	function setId_alumno($id_alumno){
		$this->id_alumno=$id_alumno;
	}

	function setId_alumnodos($id_alumnodos){
		$this->id_alumnodos=$id_alumnodos;
	}

	function setRelacion($relacion){
		$this->relacion=$relacion;
	}

	public function save(){
		$sql = "INSERT INTO relaciones VALUES (NULL, '{$this->getId_alumno()}', '{$this->getId_alumnodos()}', '{$this->getRelacion()}')";
		$save= $this->db->query($sql);
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	function relaciones(){
		$sql="SELECT relaciones.id_relacion, relaciones.id_alumno AS identificador, relaciones.id_alumnodos, alumnos.nombre AS nombreAmigo, alumnos.apellidos AS apellidoAmigo, relaciones.relacion AS tipo FROM relaciones JOIN alumnos ON relaciones.id_alumnodos=alumnos.id_alumno";
		$relaciones= $this->db->query($sql);
		//var_dump($relaciones);
		//die();
		return $relaciones;
	}

	public function eliminar(){
		$sql="DELETE FROM relaciones WHERE id_relacion={$this->id_relacion}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}



}

?>