<?php 

Class posicion{

	private $id_posicion;
	private $id_alumno;
	private $coordx;
	private $coordy;
	private $guardado;



	private $db;

	public function __construct() {
		$this->db = Database::connect();
		}

	function getId_posicion(){
		return $this->id_posicion;
	}

	function getId_alumno(){
		return $this->id_alumno;
	}

	function getCoordx(){
		return $this->coordx;
	}

	function getCoordy(){
		return $this->coordy;
	}

	function getGuardado(){
		return $this->guardado;
	}


	function setId_posicion($id_posicion){
		$this->id_posicion=$id_posicion;
	}

	function setId_alumno($id_alumno){
		$this->id_alumno=$id_alumno;
	}

	function setCoordx($coordx){
		$this->coordx=$coordx;
	}

	function setCoordy($coordy){
		$this->coordy=$coordy;
	}

	function setGuardado($guardado){
		$this->guardado=$guardado;
	}

	function crearPosicion(){
		$sql = "INSERT INTO posiciones VALUES (NULL, {$this->getId_alumno()}, {$this->getCoordx()}, {$this->getCoordy()}, 'sin')";
		$save= $this->db->query($sql);
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	function mover(){
		$sql1="DELETE FROM posiciones WHERE id_alumno={$this->id_alumno} AND guardado='sin'";
		$sql2 = "INSERT INTO posiciones VALUES (NULL, {$this->getId_alumno()}, {$this->getCoordx()}, {$this->getCoordy()}, 'sin')";
		$save1= $this->db->query($sql1);
		$save2= $this->db->query($sql2);
		$result=false;
		if ($save1 && $save2) {
			$result=true;
		}
		return $result;
	}

	function posiciones(){
		$sql="SELECT * FROM posiciones WHERE guardado='sin'";
		$save= $this->db->query($sql);
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

}



 ?>