<?php 


class aula{

	private $id_aula;
	private $codigo;
	private $mesas;
	
	private $db;

	public function __construct() {
		$this->db = Database::connect();
	}


	function getId_aula(){
		return $this->id_aula;
	}
	function getCodigo(){
		return $this->codigo;
	}
	function getMesas(){
		return $this->mesas;
	}
	
	function setId_aula($id_aula){
		$this->id_aula=$id_aula;
	}

	function setCodigo($codigo){
		$this->codigo=$codigo;
	}

	function setMesas($mesas){
		$this->mesas=$mesas;
	}

	function getPupitres(){
		$id_aula=$this->getId_aula();
		$pupitres= $this->db->query("SELECT mesas FROM aulas WHERE id_aula=$id_aula;");
		return $pupitres;
	}

	function aulas(){
		$sql="SELECT * FROM aulas";
		$aulas= $this->db->query($sql);
		return $aulas;
	}
	function unAula(){
		$sql="SELECT * FROM aulas WHERE id_aula={$this->id_aula}";
		$aula= $this->db->query($sql);
		
		return $aula;
	}

	public function save(){
		$sql = "INSERT INTO aulas VALUES (NULL, '{$this->getCodigo()}', '{$this->getMesas()}')";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	function borrarAula(){
		$sql="DELETE FROM aulas WHERE id_aula={$this->id_aula}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}





} 
?>